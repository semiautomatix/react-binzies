import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import { Alert, Tab, Tabs } from 'react-bootstrap';
import qs from 'qs';
import ItemsCarousel from 'react-items-carousel';
import { FaAngleLeft, FaAngleRight, FaAngleDoubleDown } from 'react-icons/fa';
import { Motion, spring } from 'react-motion';
import ScrollableAnchor from 'react-scrollable-anchor';
import './studio.css';

// images
import bg from './images/studio-BG-wall.jpg';
import bin from './images/bin.png';
import eyes1 from './images/Eyes-01.png';
import eyes2 from './images/Eyes-02.png';
import eyes3 from './images/Eyes-03.png';
import eyes4 from './images/Eyes-04.png';
import eyes5 from './images/Eyes-05.png';
import eyes6 from './images/Eyes-06.png';
import mouthes7 from './images/Mouths-07.png';
import mouthes8 from './images/Mouths-08.png';
import mouthes9 from './images/Mouths-09.png';
import mouthes10 from './images/Mouths-10.png';
import mouthes11 from './images/Mouths-11.png';
import mouthes12 from './images/Mouths-12.png';
import mouthes13 from './images/Mouths-13.png';
import accessories20 from './images/Untitled-5-20.png';
import accessories21 from './images/Untitled-5-21.png';
import accessories22 from './images/Untitled-5-22.png';
import accessories23 from './images/Untitled-5-23.png';
// import accessories24 from './images/Untitled-5-24.png';
import accessories25 from './images/Untitled-5-25.png';
import words14 from './images/Words-14.png';
import words15 from './images/Words-15.png';
import words16 from './images/Words-16.png';
import words17 from './images/Words-17.png';
import words18 from './images/Words-18.png';
import words19 from './images/Words-19.png';
import myBinzie from './images/mybinzie.jpg';
import binzieButton from './images/binziebutton.jpg';

const Container = styled.div`
	display: flex;
	flex-direction: column;
	background: url(${bg}) no-repeat center center; 
	background-size: auto 100%;
	background-color: #1a191a;
	@media (max-width: 600px) {
		background: url(${bg}) center bottom; 
		background-size: auto 100vh;
	}		
`;

const FloatingAlert = styled(Alert)`
	position: absolute;
	width: 100%;
	top: 0;
	z-index: 100;
`;

const StudioContainer = styled.div`
	display: flex;
	flex-direction: row;
	height: 80vh;
	min-height: 100%;
	max-height: 100%;
	width: 100%;
	margin: auto;	
	position: relative;
	@media (max-width: 600px) {
		flex-direction: column;
		height: 180vh;
	}				
`;

const ImageContainer = styled.div`
	display: flex;
	flex: 0.6;
	align-items: center;
	justify-content: center;	
	@media (max-width: 500px) {
		flex: 1;
	}								
`;

const RightContainer = styled.div`
	display: flex;
	flex: 0.4;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	max-width: 35vh;
	padding-left: 5vh;
	margin-right: 10vh;
	@media (max-width: 600px) {
		flex: 1;
	}				
	@media (min-height: 1024px) {
		margin-right: 3vh;
	}					
`;

const MyBinzieContainer = styled.div`
	background-color: white;
	margin: auto;
	padding: 2vh;
	@media all and (max-height: 800px) and (max-width: 600px){
		width: 40vw;
	}					
	@media screen and (orientation:landscape) {
		width: 30vw;
	}		
	@media screen and (orientation:portrait) {
		width: 80vw;
	}		
`;

const MenuImage = styled.img`
	bottom: 35%;
	margin: auto;
	padding: 10%;
	max-height: 10vh;
	max-width: 15vw;
	z-index: 10;
	@media screen and (orientation:portrait) and (max-width: 600px) {
		max-width: 25vw;
		height: auto;
	}		
	@media screen and (orientation:landscape) {
		padding: 0%;		
	}
`;

// 0.6877
const BinImage = styled.img`
	position: absolute;
	margin: auto;
	height: 50vh;
	z-index: 1;
	@media (max-width: 600px) {
		top: 15vh;
	}		
	@media screen and (orientation:landscape) {
		bottom: 10%;
	}		
`;

const EyesImage = styled.img`
	position: absolute;	
	margin: auto;
	padding-left: 7vh;
	height: 13vh;
	z-index: 10;
	@media (max-width: 600px) {
		top: -40vh;
	}		
	@media (min-width: 600px) {
		bottom: 40%;
	}		 
`;

const MouthesImage = styled.img`
	position: absolute;
	margin: auto;
	padding-left: 7vh;
	height: 13vh;
	z-index: 10;
	@media (max-width: 600px) {
		top: -40vh;
	}		
	@media (min-width: 600px) {
		bottom: 40%;
	}		 	 
`;

const AccessoriesImage = styled.img`
	position: absolute;
	margin: auto;
	padding-left: 7vh;
	height: 13vh;
	z-index: 10;
	@media (max-width: 600px) {
		top: -40vh;
	}		
	@media (min-width: 600px) {
		bottom: 15%;
	}		
`;

const WordsImage = styled.img`
	position: absolute;
	margin: auto;
	padding-left: 7vh;
	height: 4.5vh;
	z-index: 10;
	@media (max-width: 600px) {
		top: -35vh;
	}		
	@media (min-width: 600px) {
		bottom: 20%;
	}		
`;

const MyBinziesImg = styled.img`
	height: 10vh;
	max-width: 100%;
	margin: auto;
	margin-bottom: 20px;	
`;

const TextSpan1 = styled.span`
	color: #539dd0;
	font-weight: bolder;
	text-align: left;
	max-width: 25vh;
	width: 25vh;
	@media (max-height: 800px) {
		max-width: 30vh;
		width: 30vh;
	}			
`;

const TextSpan2 = styled.span`
	color: #a4a1a1;
	font-weight: bolder;
	text-align: left;
	max-width: 25vh;
	width: 25vh;
	@media (max-height: 800px) {
		max-width: 30vh;
		width: 30vh;
	}	
`;

const TextField = styled.input`
	border-radius: 0px;
	border-width: 3px;
	border-color: #848484;
	color: #848484;
	max-width: 25vh;
	margin-bottom: 20px;	
	@media (max-height: 800px) {
		max-width: 90%;		
	}
	@media (max-height: 400px) {		
		margin-bottom: 0px;
	}	
	@media (max-width: 600px) {
		max-width: 90%;
	}	
`;

const BinzieButton = styled.button`
	background: url(${binzieButton}) no-repeat center;
	background-size:  20vh 65px;
	max-width: 25vh;
	width: 25vh;
	height: 65px;	
	border: 0px;
	@media screen and (orientation:portrait) {
		background: url(${binzieButton}) no-repeat center;
		background-size: 30vh 50px;
		max-width: 35vh;
		width: 35vh;		
	}		
`;

const NextContainer = styled.div`
	position: absolute;
	top: 70vh;
	left: 43vw;
	@media screen and (orientation:landscape) {
		display: none;
	}
`;

const Next = styled(FaAngleDoubleDown)`
	color: white;
	font-size: 14vw;	
`;

const CarouselContainer = styled.div`
	background-color: white;
	padding-left: 25%;
	padding-right: 25%;
	@media (max-width: 600px) {
		padding-left: 0vw;
		padding-right: 0vw;
	}
`;

// eslint-disable-next-line
const Arrow = (position) => {
	if (position === 'left') {
		return <FaAngleLeft style={{ fontSize: 48, color: 'black' }} />;
	}
	return <FaAngleRight style={{ fontSize: 48, color: 'black' }} />;
};

const ArrowLeft = Arrow('left');
const ArrowRight = Arrow('ryght');

class Studio extends Component {
	// handleClick = this.handleClick.bind(this);
	static eyes = [
		{
			key: 'Furry Blue',
			value: eyes1,
		},
		{
			key: 'Orange Surprise',
			value: eyes2,
		},
		{
			key: 'Three Looks',
			value: eyes3,
		},
		{
			key: 'Purple Googlies',
			value: eyes4,
		},
		{
			key: 'One Eye on You',
			value: eyes5,
		},
		{
			key: 'Green Eyed Glare',
			value: eyes6,
		},
	];

	static mouthes = [
		{
			key: 'Slurpy Tongue',
			value: mouthes7,
		},
		{
			key: 'Wiggly Growl',
			value: mouthes8,
		},
		{
			key: 'Open Wide',
			value: mouthes9,
		},
		{
			key: 'Toothy Grin',
			value: mouthes10,
		},
		{
			key: 'Green Pointers',
			value: mouthes11,
		},
		{
			key: 'Drooly Goo',
			value: mouthes12,
		},
		{
			key: 'Thin Lipped Think',
			value: mouthes13,
		},
	];

	static accessories = [
		{
			key: 'Banana Hat',
			value: accessories20,
			position: 'top',
		},
		{
			key: 'Green Galoshes',
			value: accessories21,
			position: 'bottom',
		},
		{
			key: 'Mexican Shade',
			value: accessories22,
			position: 'top',
		},
		{
			key: 'Fluffy Moustache',
			value: accessories23,
			position: 'middle',
		},
		{
			key: 'Crazy Wild Hair',
			value: accessories25,
			position: 'top',
		},
	];

	static words = [
		{
			key: 'HOWZIT!',
			value: words14,
		},
		{
			key: 'YOLO',
			value: words15,
		},
		{
			key: 'EISH!',
			value: words16,
		},
		{
			key: 'HUNGRY!',
			value: words17,
		},
		{
			key: 'feed me!',
			value: words18,
		},
		{
			key: 'NOMNOM',
			value: words19,
		},
	];

	state = {
		eyes: Studio.eyes[0].key,
		mouth: Studio.mouthes[0].key,
		accessory: Studio.accessories[0].key,
		name: '',
		word: Studio.words[0].key,
		number: '',
		errors: [],
		editable: false,
		menuType: 'eyes',
		scale: 1,
		key: 'eyes',
		activeItemIndex: 0,
	};

	static getDerivedStateFromProps = (props) => {
		const editable = !(props.location && props.location.pathname.includes('/view'));
		const params = editable ? [] : qs.parse(props.location.search.slice(1));
		if (!editable) {
			params.eyes = Studio.eyes.find((obj) => obj.key === params.eyes);
			params.mouth = Studio.mouthes.find((obj) => obj.key === params.mouth);
			params.accessory = Studio.accessories.find((obj) => obj.key === params.accessory);
			params.word = Studio.words.find((obj) => obj.key === params.word);
		}
		return {
			editable,
			...params,
		};
	}

	componentWillMount() {
		this.setState({ scale: 1 });
	}

	componentDidMount() {
		setInterval(() => {
			this.setState({ scale: this.state.scale === 1 ? 0.75 : 1 });
		}, 1000);
	}

	onSelect = (name, key) => {
		this.setState({
			[name]: key,
		});
	}

	handleTabChange = (key) => {
		this.setState({ key, menuType: key });
	}

	handleChange = (name) => (value) => {
		this.setState({
			[name]: value,
		});
	};

	handleChange2 = (name) => (value) => {
		this.setState({
			[name]: value.target.value,
		});
	};

	handleSubmit = async () => {
		const {
			eyes,
			mouth,
			accessory,
			word,
			name,
			number,
		} = this.state;

		const errors = [];
		if (!eyes) errors.push('Please choose some eyes');
		if (!mouth) errors.push('Please choose a mouth');
		if (!accessory) errors.push('Please choose an accessory');
		if (!name) errors.push('Please choose your binzies\'a name');
		if (!word) errors.push('Please choose a favourite word');
		if (!number) errors.push('Please choose a house number');

		if (errors.length > 0) {
			this.setState({ errors });
		} else {
			// TODO env variable for URL, product number
			const url = `https://binzies.co.za/cart/?add-to-cart=1221&quantity=1&eyes=${eyes.key}&mouth=${mouth.key}&accessory=${accessory.key}&word=${word.key}&_name=${name}&number=${number}`;
			window.location.href = url;
		}
	}

	changeActiveItem = (activeItemIndex) => this.setState({ activeItemIndex });

	render() {
		const {
			eyes,
			mouth,
			accessory,
			word,
			name,
			number,
			errors,
			editable,
			menuType,
			activeItemIndex,
		} = this.state;
		const { position } = Studio.accessories.find((row) => row.key === accessory);
		const eyePos = ((pos) => {
			switch (pos) {
				case 'top':
					return '33%';
				case 'middle':
					return '45%';
				default:
					return '37%';
			}
		})(position);
		const mouthPos = ((pos) => {
			switch (pos) {
				case 'top':
					return '20%';
				case 'middle':
					return '20%';
				default:
					return '20%';
			}
		})(position);
		const accessoryPos = ((pos) => {
			switch (pos) {
				case 'top':
					return '45%';
				case 'middle':
					return '32%';
				default:
					return '12%';
			}
		})(position);
		const wordPos = ((pos) => {
			switch (pos) {
				case 'bottom':
					return '53%';
				default:
					return '15%';
			}
		})(position);

		// One item component
		// selected prop will be passed
		// eslint-disable-next-line
		const ScrollMenuItem = ({ value, list, selectedValue, type }) => (
			<div className={`menu-item ${value === selectedValue ? 'active' : ''}`}>
				<MenuImage src={list.find((row) => row.key === value).value} alt={value} onClick={() => this.onSelect(type, value)} />
			</div>
		);

		const Menu = (list, selected, type) => list.map((el) => {
			const { key } = el;
			return <ScrollMenuItem value={key} key={key} selectedValue={selected} list={list} type={type} style={{ backgroundColor: "white"}}/>;
		});

		// Create menu from items
		const menu = ((type) => {
			switch (type) {
				case 'eyes': return Menu(Studio.eyes, eyes, type);
				case 'mouth': return Menu(Studio.mouthes, mouth, type);
				case 'accessory': return Menu(Studio.accessories, accessory, type);
				case 'word': return Menu(Studio.words, word, type);
				default: return Menu(Studio.eyes, eyes, type);
			}
		})(menuType);

		const theStyle = {
			scale: spring(this.state.scale, { stiffness: 180, damping: 6 }),
		};

		return (
			<Container>
				{errors.length > 0 &&
					<FloatingAlert bsStyle="danger">
						{errors.map((error) => <Fragment>{error}<br /></Fragment>)}
					</FloatingAlert>
				}
				{editable ?
					<>
						<Tabs
							style={{backgroundColor: '#4189c8', justifyContent: 'center'}}
							id="controlled-tab-example"
							activeKey={this.state.key}
							onSelect={this.handleTabChange}
						>
							<Tab eventKey="eyes" title="EYES" />
							<Tab eventKey="mouth" title="MOUTHES" />
							<Tab eventKey="accessory" title="ACCESSORIES" />
							<Tab eventKey="word" title="WORDS" />
						</Tabs>
						<CarouselContainer>
							<ItemsCarousel
								// Placeholder configurations
								enablePlaceholder
								numberOfPlaceholderItems={5}
								minimumPlaceholderTime={1000}
								placeholderItem={<div style={{ height: 200, background: '#900' }}>Placeholder</div>}
								// Carousel configurations
								numberOfCards={3}
								gutter={12}
								showSlither
								firstAndLastGutter
								freeScrolling={false}
								// Active item configurations
								requestToChangeActive={this.changeActiveItem}
								activeItemIndex={activeItemIndex}
								activePosition="center"
								chevronWidth={24}
								rightChevron={ArrowRight}
								leftChevron={ArrowLeft}
								outsideChevron={false}
								classes={{ wrapper: "carousel-wrapper", itemsWrapper: "carousel", itemWrapper: "carousel-it" }}
							>
								{menu}
							</ItemsCarousel>
						</CarouselContainer>
					</> :
					null
				}
				<StudioContainer>
					<ImageContainer>
						<BinImage src={bin} alt="bin" />
						{eyes && <EyesImage
							src={Studio.eyes.find((row) => row.key === eyes).value}
							alt="eye"
							style={{ bottom: eyePos }}
						// onClick={() => this.setState({ menuType: 'eyes' })}
						/>}
						{mouth && <MouthesImage
							src={Studio.mouthes.find((row) => row.key === mouth).value}
							alt="mouth"
							style={{ bottom: mouthPos }}
						// onClick={() => this.setState({ menuType: 'mouth' })}
						/>}
						{accessory && <AccessoriesImage
							src={Studio.accessories.find((row) => row.key === accessory).value}
							alt="accessory"
							style={{ bottom: accessoryPos }}
						// onClick={() => this.setState({ menuType: 'accessory' })}
						/>}
						{word && <WordsImage
							src={Studio.words.find((row) => row.key === word).value}
							alt="word"
							style={{ bottom: wordPos }}
						// onClick={() => this.setState({ menuType: 'word' })}
						/>}
					</ImageContainer>
					<NextContainer>
						<Motion style={theStyle}>
							{({ scale }) => (
								<div style={{ transform: `scale(${scale})` }}>
									<a href="#myBinzie">
										<Next />
									</a>
								</div>
							)}
						</Motion>
					</NextContainer>
					<RightContainer>
						{editable &&
							<ScrollableAnchor id="myBinzie">
								<MyBinzieContainer>
									<MyBinziesImg src={myBinzie} alt="mybinzie" /><br />
									<TextSpan1>your BINZIES Name:</TextSpan1>
									<TextField
										className="form-control"
										id="nameInput"
										onChange={this.handleChange2('name')}
										value={name}

									/>
									<TextSpan1>HOUSE NUMBER:</TextSpan1><br />
									<TextSpan2>We&apos;ll send you a sticker!</TextSpan2>
									<TextField
										type="number"
										className="form-control"
										id="numberInput"
										onChange={this.handleChange2('number')}
										value={number}
									/>
									<BinzieButton onClick={this.handleSubmit} />
								</MyBinzieContainer>
							</ScrollableAnchor>}
					</RightContainer>
				</StudioContainer>
			</Container>
		);
	}
}

export default Studio;
