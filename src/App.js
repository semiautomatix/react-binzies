import styled from 'styled-components';
import './App.css';
import Studio from './Studio'

const AppWrapper = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100%;   
  zIndex: 1;
  overflow: 'hidden';
`;

function App() {
  return (
    <AppWrapper>
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
        crossOrigin="anonymous"
      />
      <Studio />
    </AppWrapper>
  );
}

export default App;
